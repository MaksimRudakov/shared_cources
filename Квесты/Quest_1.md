### Созадем gateway virtual machine на ubuntu-server 24.04

Для начала с помощью Virtual Network Editor нам необходимо настроить сети:
- VMnet8 (NAT)
```yaml
name: "VMnet8"
type: "NAT"
subnet_ip: 192.168.23.0
subnet_mask: 255.255.255.0
dhcp: true
```
- internal1 (Host-only)
```yaml
name: "internal1"
type: "Host-only"
subnet_ip: 192.168.20.0
subnet_mask: 255.255.255.0
dhcp: false
```

![][./images/virtual-network-editor.png]

Создаем виртуальную машину на базе [ubuntu-server 24.04]([Ubuntu Server - for scale out workloads | Ubuntu](https://ubuntu.com/server))
1. Скачиваем iso ubuntu;
2. Создаем виртуальную машину в VMware Wokstation
	![][./images/new-virtual-machine.png]]

3. Добавляем к виртуалке два network adapter, которые подключены к сетям VMnet8 и internal1;
	 ![][./images/virtual-machine-settings.png]]
	 
4. Запускаем виртуальную машину и настраиваем.


Далее нам необходимо настроить оба сетевых интерфейса с помощью netplan.
[Мануал](https://serverspace.io/support/help/multiple-network-interfaces-ubuntu-20-04/)

Пример:
```yaml
network:
    ethernets:
        ens33:
            addresses:
            - 192.168.23.10/24
            nameservers:
                addresses:
                - 8.8.8.8
                search: []
            routes:
            -   to: default
                via: 192.168.23.2
        ens37:
            addresses:
            - 192.168.20.10/24
    version: 2
```

#### *
Рассказать что такое `mtu: 1500`

#### Дополнительно
- Прочитать про команду `ip` в Linux;
- Прочитать что такое `netplan`.
